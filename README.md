# Docker-VirSorter-to-vContact

## Overview

**VirSorter-to-vContact** is a python-wrapper for a series of steps required to transform output produced by VirSorter to input consumed by vContact. Designed to be as user-friendly as possible, all that is required from the user is to have a VirSorter data folder generated from their data. This tool will then perform the following functions (each *function* is already implemented as separate apps in the [Cyverse](http://www.cyverse.org/) discovery environment):

* Generate category-specific nucleotide and protein versions of viral-identified sequences
* Generate the gene-to-contig mapping file required by vContact-PCs
* Can incorporate additional reference datasets alongside viral-identified sequences (includes combined gene-to-contig mapping and  proteins files)
* Creates a BLAST database using above protein sequence file, then BLASTs against the same proteins to create the all-protein-vs-protein comparison required by vContact-PCs
* Incorporates functional annotations found from vContact's pfam search (includes reference dataset annotations too)


## Installation

Since everything runs within a Docker container, there is no need to install any dependencies, other than [Docker](https://www.docker.com/).

### Using the Dockerfile

Build the Docker image from the Dockerfile:

```
docker build -t bbolduc/virsorter_to_vcontact:dev .
```

The "." in the code above means to use the Dockerfile from the current directory. (the file is literally named "Dockerfile")

Now you can "run" the Docker image - almost as if it's another program on your system (typical run command).

```
docker run --rm -v /path/to/virsorter/dir:/inputDir -w /inputDir bbolduc/virsorter_to_vcontact:dev --virsorter-dir /inputDir --additional-dbs 'ViralRefSeq-72' -c 1 -c 2 --log-file logfile.log
```

Note: Commands after the "bbolduc/virsorter_to_vcontact:dev" are being passed to the Dockerized python script.

### Minimal command line options

**--rm**: Remove the Docker container after running.

**-v**: Bind directory from the *host* system to the container system. This is the **absolute host path**, followed by **:**, then where (on the *container's* system) to mount it.

**-w**: Sets working directory *within the container*.

**--dir**: Tells the script *where* the VirSorter directory is located. For ease of use, this **must be** the same directory that is mounted under the container.

### Other Options

**--log**: Log file name.

**--additional-dbs**: Number of threads to use during processing.

**--categories**: VirSorter categories (measures of confidence in viral sequence assessment) to consider. This argument must be repeated to include multiple categories, e.g. --categories 1 --categories 2 --categories 4.

There are currently 2 "additional dbs" included: NCBI's Viral Reference Sequence Database (Version 72) and "Viromes," which includes the viral refseq db and [additional viruses identified by VirSorter](http://elifesciences.org/content/4/e08490v3/abstract) from publicly available databases.


### Future Updates

* Incorporating taxonomic affiliations with VirSorter's pfam domain hits

### Additional Comments

vContact (the tool fed the outputs from vContact-PCs) cannot incorporate taxonomic affiliations outside of the "contigs" file generated from vContact-PCs. Since vContact-PCs only takes a BLASTP and proteins file (containing *only* protein IDs and keywords), there is no (read: not automatic) way to pass the taxonomic information from *before* vContact-PCs to vContact. Therefore, while the eventual output of **VirSorter-to-vContact** will include taxonomic levels for each sequence in the network, it will not include cluster-level taxonomic precision calling. Future updates to vContact will resolve this issue.

### Authors

* Ben Bolduc