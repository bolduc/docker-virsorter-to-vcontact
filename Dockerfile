FROM ubuntu:14.04.3
MAINTAINER Benjamin Bolduc <bolduc.10@osu.edu>

# Remove those annoying warnings
ENV DEBIAN_FRONTEND noninteractive

# Set up bin
ENV BINPATH /usr/bin

# Install essentials. Suggested that should build python from source...
RUN apt-get update && apt-get install -y \
    automake \
    build-essential \
    wget \
    libsqlite3-dev \
    python \
    python-dev

# Download and install dependencies
# BLAST
RUN wget --no-verbose ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.4.0/ncbi-blast-2.4.0+-x64-linux.tar.gz
RUN tar zxf ncbi-blast-2.4.0+-x64-linux.tar.gz && cd ncbi-blast-2.4.0+ && chmod +x bin/* && cp bin/* $BINPATH

# Install pip
RUN wget --no-verbose https://bootstrap.pypa.io/get-pip.py
RUN python get-pip.py

# Install biopython & ete2
RUN pip install biopython
RUN pip install ete2

# Clean stuff up
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# No longer using COPY, instead favoring ADD to allow for 'pure' use of Dockerfile, rather
# than requiring users to download the whole repo
RUN mkdir /DBs
ENV BITBUCKET_URL https://bitbucket.org/bolduc/docker-virsorter-to-vcontact/raw/e4c7f06fe4f5bef8122d188d1974d306af2a5def/
#COPY DBs/ /DBs
#ADD $BITBUCKET_URL/DBs/ViralRefSeq_72.faa.gz /DBs/
#ADD $BITBUCKET_URL/DBs/ViralRefSeq_72_and_VirSorted.faa.gz /DBs/
RUN wget --no-verbose -P /DBs/ $BITBUCKET_URL/DBs/ViralRefSeq_72.faa.gz
RUN wget --no-verbose -P /DBs/ $BITBUCKET_URL/DBs/ViralRefSeq_72_and_VirSorted.faa.gz

# Copy scripts to system
# COPY Scripts/ $BINPATH
ADD $BITBUCKET_URL/Scripts/Gene2Contig-from-Refs.py $BINPATH/
ADD $BITBUCKET_URL/Scripts/VirSorter-to-vContact.py $BINPATH/
RUN chmod +x $BINPATH/*.py

# Copy and run parsers on DB files
RUN Gene2Contig-from-Refs.py  # Will create ViralRefSeq_72-proteins.csv, ViralRefSeq_72_and_VirSorted-proteins.csv

# Ready script for running
ENTRYPOINT ["VirSorter-to-vContact.py"]
CMD ["--help"]