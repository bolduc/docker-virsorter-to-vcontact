#!/usr/bin/env python

###############################################################################
#                                                                             #
#    Gene2Contig-from-Refs                                                    #
#                                                                             #
#    A support script that parses reference databases used by                 #
#    VirSorter-to-vContact.                                                   #
#                                                                             #
#    Copyright (C) Benjamin Bolduc                                            #
#                                                                             #
###############################################################################
#                                                                             #
#    This library is free software; you can redistribute it and/or            #
#    modify it under the terms of the GNU Lesser General Public               #
#    License as published by the Free Software Foundation; either             #
#    version 3.0 of the License, or (at your option) any later version.       #
#                                                                             #
#    This library is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        #
#    Lesser General Public License for more details.                          #
#                                                                             #
#    You should have received a copy of the GNU Lesser General Public         #
#    License along with this library.                                         #
#                                                                             #
###############################################################################

import os
from Bio import SeqIO
import re
import csv
import gzip
from ete2 import NCBITaxa

ncbi = NCBITaxa()


def parseRefSeq(seqRecord, geneAnnotationDict=dict, contigMappingDict=dict):
    """"""
    record_id = seqRecord.id
    record_desc = seqRecord.description

    record_organism = re.findall(r'\[([^]]*)\]', record_desc)
    record_organism = record_organism[-1]

    if record_organism not in contigMappingDict:
        contigMappingDict[record_organism] = [record_id]
    else:
        contigMappingDict[record_organism].append(record_id)

    geneAnnotationDict[record_id] = record_desc.split(' ', 1)[1].split('[')[0].strip()


def parseVirSortedSeq(seqRecord, geneAnnotationDict=dict, contigMappingDict=dict):

    record_id = seqRecord.id  # There is only an id
    record_contig = record_id.rsplit('_', 2)[0]

    if record_contig not in contigMappingDict:
        contigMappingDict[record_contig] = [record_id]
    else:
        contigMappingDict[record_contig].append(record_id)

    geneAnnotationDict[record_id] = record_contig.split('_')[0].strip('_')  # Class or Family name + pesky 2x _ around


def parseX(string):
    pass

if __name__ == '__main__':

    # Docker has already created *-proteins.csv files in the /DBs directory (alongside the existing
    # *.faa DB files.

    referenceDataSets = [
        '/DBs/ViralRefSeq_72.faa.gz',
        '/DBs/ViralRefSeq_72_and_VirSorted.faa.gz'
    ]

    use_accession = False

    # Can either be 'smart' and parse a combined reference file, then split and (potentially) re-join DBs that should be
    # re-joined, or, parse each reference file separately, even if that means re-parsing the exact same parts of the
    # file (i.e. _and_ files are concatenated versions)

    for referenceDataSet in referenceDataSets:

        contigMapping = {}
        geneAnnotation = {}

        with gzip.open(referenceDataSet, 'rU') as referenceDataSet_fh:

            for record in SeqIO.parse(referenceDataSet_fh, 'fasta'):
                if 'gi|' in record.id:  # It is refSeq
                    parseRefSeq(record, geneAnnotation, contigMapping)
                elif ('_gi_' in record.id) or ('_gene_' in record.id):
                    parseVirSortedSeq(record, geneAnnotation, contigMapping)
                else:
                    print('Error: {}'.format(record.description))

        referenceDataSet_csv = os.path.join(os.path.dirname(referenceDataSet),
                                            os.path.basename(referenceDataSet).rsplit('.', 2)[0] + '-proteins.csv')

        with open(referenceDataSet_csv, 'w') as referenceDataSet_csv_fh:
            csvWriter = csv.writer(referenceDataSet_csv_fh, delimiter=',', quotechar='"')

            csvWriter.writerow(['id', 'contig', 'keywords'])

            for organism, genes in contigMapping.items():

                # Need taxonomy
                try:
                    taxID = ncbi.get_name_translator([organism])
                except IndexError:  # It's VirSorted
                    try:
                        taxID = ncbi.get_name_translator([organism.split('_')[0]])
                    except:
                        print('error', organism.split('_')[0])
                try:
                    lineage = ncbi.get_lineage(taxID.values()[0][0])  # Had 2 [0] there
                except IndexError:
                    lineage = [1, 10239]

                names = ncbi.get_taxid_translator(lineage)  # Dictionary

                sorted_ranked_names = {}
                for tax_id, lineage_name in names.items():
                    rank_name = ncbi.get_rank([tax_id])
                    if rank_name.values()[0] != 'no rank':
                        sorted_ranked_names[rank_name.values()[0]] = lineage_name

                if len(sorted_ranked_names) > 0:
                    lineages = ';'.join(sorted_ranked_names.values())
                else:
                    lineages = 'None'

                # Fix "naming" issue, cuz vContact can't apparently handle spaces in name
                organism = organism.replace(' ', '~')

                for gene in genes:

                    if use_accession:  # gi|157952305|ref|YP_001497197.1|
                        organism = gene.split('|')[-2]

                    csvWriter.writerow([gene, organism, ';'.join([lineages, geneAnnotation[gene]])])
