#!/usr/bin/env python

###############################################################################
#                                                                             #
#    VirSorter-to-vContact                                                    #
#                                                                             #
#    A wrapper script, written for Docker, that extracts and compiles         #
#    multiple levels of information from a VirSorter directory, preparing the #
#    data for use in vContact-PCs. This includes preparing BLAST databases,   #
#    performing the requisite BLAST, and optionally including reference       #
#    datasets.                                                                #
#                                                                             #
#    Copyright (C) Benjamin Bolduc                                            #
#                                                                             #
###############################################################################
#                                                                             #
#    This library is free software; you can redistribute it and/or            #
#    modify it under the terms of the GNU Lesser General Public               #
#    License as published by the Free Software Foundation; either             #
#    version 3.0 of the License, or (at your option) any later version.       #
#                                                                             #
#    This library is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        #
#    Lesser General Public License for more details.                          #
#                                                                             #
#    You should have received a copy of the GNU Lesser General Public         #
#    License along with this library.                                         #
#                                                                             #
###############################################################################

__author__ = "Ben Bolduc"
__copyright__ = "Copyright 2016"
__credits__ = ["Ben Bolduc"]
__license__ = "LGPLv3"
__maintainer__ = "Ben Bolduc"
__email__ = "bolduc.10@osu.edu"
__status__ = "Development"

import sys
import os
import argparse
import csv
import subprocess
import multiprocessing
from Bio import SeqIO
import gzip
from ete2 import NCBITaxa


parser = argparse.ArgumentParser(
    description='This script takes a VirSorter directory and parses it to generate all the necessary files for vContact',
    formatter_class=argparse.RawTextHelpFormatter)

options = parser.add_argument_group('Options')
options.add_argument('-d', '--virsorter-dir', dest='virsorter_dir', metavar='FILENAME',
                     default='/Users/bolduc.10/Research/iVirus/VirSorter_1.0.3_analysis1-2016-03-09-17-18-08.9')
options.add_argument('-a', '--additional-dbs', dest='db', choices=['ViralRefSeq-72', 'VirSorter'],
                     help='Select additional databases to include in the vContact analysis.')
options.add_argument('-c', '--categories', action='append', default=['1', '2'],
                     help='VirSorter \'categories\' to include in subsequent analysis. For example, to include '
                          'category 1 (quite confident) and category 2 (pretty sure), -c 1 -c 2. Keep in mind that '
                          'categories 4, 5 and 6 are predicted prophage elements.')
options.add_argument('-l', '--log-file', dest='log_fn', metavar='FILENAME', default='VirSorter-to-vContact_PCs.log',
                    help="Log file name")
options.add_argument('-t', '--num-threads', dest='threads',default=multiprocessing.cpu_count(),
                     help='Number of threads to use for BLASTP.')
options.add_argument('-e', '--evalue', default=1E-3, help='Evalue to use for BLASTP')

results = parser.parse_args()


def error(msg):
    sys.stderr.write("ERROR: {}\n".format(msg))
    sys.stderr.flush()
    sys.exit(1)

ncbi = NCBITaxa()


# Parse file
def parse_global_phage_signal(VirSorter_csv):
    categories = {}
    category = 0
    with open(VirSorter_csv, 'rU') as VirSorter_csv_fh:
        for lines in VirSorter_csv_fh:
            if lines.startswith('## ') and not lines.startswith('## Contig_id'):
                category = lines[3]

                if category not in categories:
                    categories[category] = []

            elif not lines.startswith('## Contig_id'):
                categories[category].append(lines.split(',')[0])

    # Remove categories that aren't desired
    final_categories = {key: value for key, value in categories.items() if key in results.categories}

    return final_categories


def execute(command):

    print('Executing {}'.format(command))
    process = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                               shell=True)
    (stdout, stderr) = process.communicate()

    return stdout, stderr


if __name__ == '__main__':

    # Going into this, Docker has already created *-proteins.csv files in the /DBs directory (alongside the existing
    # *.faa DB files.

    log = open(results.log_fn, 'w')

    # Source directory - should mount VirSorter directory
    virsorter_dir = os.getcwd()

    # Set up top-level directory
    work_dir = os.path.join(virsorter_dir, 'VirSorter-2-vContact-PCs')
    if not os.path.exists(work_dir):
        os.makedirs(work_dir)
        log.write('Created VirSorter-2-vContact-PCs directory: {}\n'.format(work_dir))

    # Set up 'output' directory to be used with vContact PCs (some of initial inputs need to go there)
    vcontact_pcs_dir = os.path.join(work_dir, 'vContact-PCs_input/')
    if not os.path.exists(vcontact_pcs_dir):
        os.makedirs(vcontact_pcs_dir)
        log.write('Created vContact-PCs input directory: {}\n'.format(vcontact_pcs_dir))

    # Need to combine protein FASTA file with user's as well as pre-parsed *_proteins.csv
    # Decompress and create tmpfile that's used henceforth, or only when needed
    db_faa = False
    db_gene2contig = False
    if results.db:
        if results.db == 'ViralRefSeq-72':
            db_faa = '/DBs/ViralRefSeq_72.faa.gz'
            db_gene2contig = '/DBs/ViralRefSeq_72-proteins.csv'
        if results.db == 'VirSorter':
            db_faa = '/DBs/ViralRefSeq_72_and_VirSorted.faa.gz'
            db_gene2contig = '/DBs/ViralRefSeq_72_and_VirSorted-proteins.csv'

    # Determine category for all viral sequences
    # Should = Docker working directory
    global_signals_fn = os.path.join(virsorter_dir, 'VIRSorter_global-phage-signal.csv')

    viralCatDict = parse_global_phage_signal(global_signals_fn)

    # Simply phageDict to only include contigs -> removes an iteration
    viralGenomes = set(sorted({x for v in viralCatDict.values() for x in v}))

    # Grab nucleotide sequences
    genomes_fn = os.path.join(virsorter_dir, 'Fasta_files/VIRSorter_nett_filtered.fasta')

    viral_genomes = []
    with open(genomes_fn, 'rU') as genomes_fh:
        for record in SeqIO.parse(genomes_fh, 'fasta'):

            if record.id in viralGenomes:
                viral_genomes.append(record)

    # Output 'filtered' genomes file
    viral_genomes_fn = os.path.join(vcontact_pcs_dir, 'VIRSorter_viral_nucl.fna')
    with open(viral_genomes_fn, 'w') as viral_genomes_fh:
        SeqIO.write(viral_genomes, viral_genomes_fh, 'fasta')

    # Grab protein sequences
    proteins_fn = os.path.join(virsorter_dir, 'Fasta_files/VIRSorter_prots.fasta')

    # Get potential annotations from pfam domain matches for Gene2Contig file
    affi_fn = os.path.join(virsorter_dir, 'Metric_files/VIRSorter_affi-contigs.tab')

    geneAnnotation = {}
    with open(affi_fn, 'rU') as affi_fh:
        csvReader = csv.reader(filter(lambda row: row[0] != '>', affi_fh), delimiter='|')

        for lines in csvReader:
            gene, phageCluster, pfamHit = lines[0], lines[5], lines[9]

            annots = []
            if phageCluster != '-':
                annots.append(phageCluster)
            if pfamHit != '-':
                annots.append(pfamHit)

            if len(annots) > 1:
                geneAnnotation[gene] = ';'.join(annots)
            if len(annots) == 1:
                geneAnnotation[gene] = annots[0]
            if len(annots) == 0:
                geneAnnotation[gene] = 'Unknown'

    contigMapping = {}  # Save for later
    viral_proteins = []
    with open(proteins_fn, 'rU') as proteins_fh:
        for record in SeqIO.parse(proteins_fh, 'fasta'):

            contig = record.id.split('-gene_')[0]
            if contig in viralGenomes:
                viral_proteins.append(record)

                if contig not in contigMapping:
                    contigMapping[contig] = [record.id]
                else:
                    contigMapping[contig].append(record.id)

    # Output 'filtered' proteins file
    viral_proteins_fn = os.path.join(vcontact_pcs_dir, 'VIRSorter_viral_prots.faa')
    with open(viral_proteins_fn, 'w') as viral_proteins_fh:
        SeqIO.write(viral_proteins, viral_proteins_fh, 'fasta')

    # Output merged (filtered + reference DB) proteins file
    viral_proteins_and_db_fn = False
    if results.db:
        with gzip.open(db_faa, 'rU') as db_faa_fh:
            viral_refseq_proteins = [record for record in SeqIO.parse(db_faa_fh, 'fasta')]

        viral_proteins_and_db_fn = os.path.join(vcontact_pcs_dir, 'VIRSorter_viral_prots_with_Refs.faa')
        with open(viral_proteins_and_db_fn, 'w') as viral_proteins_and_db_fh:
            SeqIO.write(viral_proteins, viral_proteins_and_db_fh, 'fasta')
            SeqIO.write(viral_refseq_proteins, viral_proteins_and_db_fh, 'fasta')

    # Generate Gene2Contig mapping file
    gene2contig_fn = os.path.join(vcontact_pcs_dir, 'VIRSorter-proteins.csv')
    # tmp_fh, tmp_fn = tempfile.mkstemp(text=True) os.fdopen(tmp_fh, 'w') as tmp_fh

    gene2contig_combined_fn = os.path.join(vcontact_pcs_dir, 'VIRSorter-proteins_with_Refs.csv')
    with open(gene2contig_fn, 'w') as gene2contig_fh, open(gene2contig_combined_fn, 'w') as gene2contig_combined_fh:
        csvWriter = csv.writer(gene2contig_fh, delimiter=',', quotechar='"')
        csvCombinedWriter = csv.writer(gene2contig_combined_fh, delimiter=',', quotechar='"')

        csvWriter.writerow(['id', 'contig', 'keywords'])

        if results.db:
            csvCombinedWriter.writerow(['id', 'contig', 'keywords'])

        for contig, genes in contigMapping.items():
            for gene in genes:
                csvWriter.writerow([gene, contig, geneAnnotation[gene]])

                if results.db:
                    csvCombinedWriter.writerow([gene, contig, geneAnnotation[gene]])

        if results.db:
            with open(db_gene2contig, 'rU') as db_gene2contig_fh:
                next(db_gene2contig_fh)  # Header
                for line in db_gene2contig_fh:
                    gene2contig_combined_fh.write(line)

    # Create BLAST DB
    makeblastdb_dir = os.path.join(work_dir, 'makeblastdb_dir/')

    if not os.path.exists(makeblastdb_dir):
        os.makedirs(makeblastdb_dir)
        log.write('Created blast db directory: {}\n'.format(makeblastdb_dir))

    source_faa_fn = viral_proteins_fn  # If no 'merged' DB, then just go with the 'original' viral proteins file
    if results.db:
        source_faa_fn = viral_proteins_and_db_fn

    source_db = os.path.join(makeblastdb_dir, os.path.basename(source_faa_fn).rsplit('.', 1)[0])

    makeblastdb_cmd = 'makeblastdb -dbtype prot -in {} -input_type fasta -hash_index -out {}'.format(source_faa_fn, source_db)

    makeblast_out, makeblast_err = execute(makeblastdb_cmd)

    # Run BLASTP on source
    blastp_fn = os.path.join(vcontact_pcs_dir, os.path.basename(source_db) + '.self-blastp.tab')
    blastp_cmd = 'blastp -task blastp -query {0} -db {1} -num_threads {2} -outfmt 6 -evalue {3} -out {4}'.format(
        source_faa_fn, source_db, results.threads, results.evalue, blastp_fn)

    blastP_out, blastP_err = execute(blastp_cmd)